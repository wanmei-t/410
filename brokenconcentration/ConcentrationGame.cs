namespace MemoryGame
{
    public class ConcentrationGame
    {

        private ConcentrationGrid _grid;
        private Player _activePlayer;
        private Player _inactivePlayer;
        public static void Main(string[] args)
        {
            ConcentrationGame game = new ConcentrationGame();
            game.RunGame();
        }


        //The constructor sets up a new game of Concentration
        public ConcentrationGame()
        {
            _grid = new ConcentrationGrid();
            Console.WriteLine("Welcome to Concentration, the matching game!");
            Console.WriteLine("Player 1, enter your name:");
            _activePlayer = new Player(GetValidInput());
            Console.WriteLine("Player 2, enter your name:");
            _inactivePlayer = new Player(GetValidInput());
        }

        //Get non-null or empty string input
        private string GetValidInput()
        {
            string? input;
            do
            {
                input = Console.ReadLine();
            // ISSUE 
            // } while (input == null);
            } while (input == null || input == "");
            return input;
        }

        // ISSUE
        private int GetValidIntInput()
        {
            do
            {
                try
                {
                    int input = Convert.ToInt32(Console.ReadLine());
                    if (input < 0 || input > 3) //I DON'T THINK THIS CAN BE HARDCODED
                    {
                        Console.WriteLine("Your answer should be 0, 1, 2 or 3.");
                    }
                    else
                    {
                        return input;
                    }
                }
                catch (FormatException e)
                {
                    Console.WriteLine("Your answer should be an integer.");
                }
            } while (true);
        }


        private void RunGame()
        {
            while (!_grid.IsEmpty())
            {
                Boolean madeMatch = DoTurn(_activePlayer);
                if (madeMatch)
                {
                    _activePlayer.addPoint();
                    Console.WriteLine($"{_activePlayer.Name} made a match! They now have {_activePlayer.Points} points");
                }
                else
                {
                    Console.WriteLine($"Sorry, no match! It is now {_inactivePlayer.Name}'s turn.");
                    // ISSUE
                    // SwitchActivePlayer();
                }
                SwitchActivePlayer();
                Console.ReadKey();
            }
            ShowResults();
        }

        internal Boolean DoTurn(Player p)
        {
            while (true)
            {
                //Create List of Guesses;
                List<Tuple<int, int>> guesses = new List<Tuple<int, int>>();
                //Ask for first guess, add it to the list
                guesses.Add(GetGuess(p, guesses));
                //Ask for second guess, add it to the list
                guesses.Add(GetGuess(p, guesses));
                //Check if the guesses are valid otherwise ask for another set of guesses
                Boolean validAnswers = _grid.AreCellsInputValid(guesses[0], guesses[1]);
                if (validAnswers)
                {
                    Console.Clear();
                    Console.WriteLine("Your guesses: ");
                    _grid.PrintGrid(guesses);
                    //Check to see if it Matches, return the result
                    return _grid.CheckMatch(guesses[0], guesses[1]);
                }
                Console.WriteLine("Your guesses are not allowed. Make sure to not choose a null card or the same card twice.");
                Console.ReadKey();
                Console.Clear();
            }
            // ISSUE
            // //Create List of Guesses;
            // List<Tuple<int, int>> guesses = new List<Tuple<int, int>>();
            // //Ask for first guess, add it to the list
            // guesses.Add(GetGuess(p));
            // //Ask for second guess, add it to the list
            // guesses.Add(GetGuess(p));
            // Console.Clear();
            // _grid.PrintGrid(guesses);
            // //Check to see if it Matches, return the result
            // return _grid.CheckMatch(guesses[0], guesses[1]);
        }

        //Prompt the user to enter a guess.
        internal Tuple<int, int> GetGuess(Player p, List<Tuple<int, int>> guesses)
        {
            Console.Clear();
            Console.WriteLine($"{p.Name}, choose a row and column to guess");
            _grid.PrintGrid(guesses);
            Console.Write("Row: ");
            int row = GetValidIntInput();
            Console.Write("Col: ");
            int column = GetValidIntInput();
            // ISSUE
            // return new Tuple<int, int>(column, row);
            return new Tuple<int, int>(row, column);
        }

        //Swaps the currently active player, making the inactive player active and vice versa
        internal void SwitchActivePlayer()
        {
            // Issue
            // _activePlayer = _inactivePlayer;
            // _inactivePlayer = _activePlayer;
            Player tempPlayer = _activePlayer;
            _activePlayer = _inactivePlayer;
            _inactivePlayer = tempPlayer;
        }

        private void ShowResults()
        {
            Console.Clear();
            Console.WriteLine($"Game over!");
            Console.WriteLine($"{_activePlayer.Name} has {_activePlayer.Points} points");
            Console.WriteLine($"{_inactivePlayer.Name} has {_inactivePlayer.Points} points");
            if (_activePlayer.Points > _inactivePlayer.Points)
            {
                Console.WriteLine($"{_activePlayer.Name} wins!");
            }
            else if (_activePlayer.Points < _inactivePlayer.Points)
            {
                Console.WriteLine($"{_inactivePlayer.Name} wins!");
            }
            //ISSUE No appropriate message for equal amount of points
            else
            {
                Console.WriteLine("There is a tie!");
            }
        }
    }
}