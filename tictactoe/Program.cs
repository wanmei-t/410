﻿namespace TicTacToe 
{
    public enum TicTacToeMarks
    {
        X,
        O,
        Empty
    }

    public class TicTacToePosition
    {
        private TicTacToeMarks _mark;
        public TicTacToeMarks Mark
        {
            get { return _mark; }
            set
            {
                if (value != TicTacToeMarks.X && value != TicTacToeMarks.O)
                {
                    throw new ArgumentException("Invalid mark.");
                }
                if (_mark == TicTacToeMarks.X || _mark == TicTacToeMarks.O)
                {
                    throw new ArgumentException("Position already marked.");
                }
                _mark = value;
            }
        }

        public TicTacToePosition()
        {
            _mark = TicTacToeMarks.Empty;
        }

        public override string ToString()
        {
            if (_mark == TicTacToeMarks.X)
            {
                return "X";
            }
            else if (_mark == TicTacToeMarks.O)
            {
                return "O";
            }
            else // _mark == TicTacToeMarks.Empty
            {
                return " ";
            }
        }
    }

    public class TicTacToeGrid
    {
        private TicTacToePosition[,] _grid;
        public string Winner
        {
            get
            {
                return CheckGrid();
            }
        }

        public TicTacToeGrid(int size)
        {
            _grid = new TicTacToePosition[size, size];
            for (int i = 0; i < _grid.GetLength(0); i++)
            {
                for (int j = 0; j < _grid.GetLength(1); j++)
                {
                    _grid[i, j] = new TicTacToePosition();
                }
            }
        }

        public Boolean PlaceCharacter(TicTacToeMarks mark, int row, int column)
        {
            if (row < 0 || row >= _grid.GetLength(0))
            {
                return false;
            }
            if (column < 0 || column >= _grid.GetLength(1))
            {
                return false;
            }
            if (_grid[row, column].Mark != TicTacToeMarks.Empty)
            {
                return false;
            }
            if (mark != TicTacToeMarks.X && mark != TicTacToeMarks.O)
            {
                return false;
            }

            _grid[row, column].Mark = mark;
            return true;
        }

        public void PrintGrid()
        {
            Console.Write("  ");
            for (int i = 0; i < _grid.GetLength(1); i++)
            {
                Console.Write("{0} ", i);
            }
            Console.WriteLine();

            for (int i = 0; i < _grid.GetLength(0); i++)
            {
                Console.Write("{0} ", i);
                for (int j = 0; j < _grid.GetLength(1); j++)
                {
                    Console.Write("{0} ", _grid[i, j]);
                }
                Console.WriteLine();
            }
        }

        public string CheckRows()
        {
            string winner = "";
            for (int i = 0; i < _grid.GetLength(0); i++)
            {
                TicTacToeMarks mark = _grid[i, 0].Mark;
                if (mark != TicTacToeMarks.Empty)
                {
                    winner = mark.ToString();
                    for (int j = 1; j < _grid.GetLength(1); j++)
                    {
                        if (mark != _grid[i, j].Mark)
                        {
                            winner = "";
                            break;
                        }
                    }
                    if (!winner.Equals(""))
                    {
                        break;
                    }
                }
            }
            return winner;
        }

        public string CheckColumns()
        {
            string winner = "";
            for (int i = 0; i < _grid.GetLength(1); i++)
            {
                TicTacToeMarks mark = _grid[0, i].Mark;
                if (mark != TicTacToeMarks.Empty)
                {
                    winner = mark.ToString();
                    for (int j = 1; j < _grid.GetLength(0); j++)
                    {
                        if (mark != _grid[j, i].Mark)
                        {
                            winner = "";
                            break;
                        }
                    }
                    if (!winner.Equals(""))
                    {
                        break;
                    }
                }
            }
            return winner;
        }

        public string CheckDiagonals()
        {
            //Assuming _grid.GetLength(0) and _grid.GetLength(1) are the same value aka a square array
            string winner = "";

            //Checking diagonal that goes from top left to bottom right
            TicTacToeMarks mark = _grid[0, 0].Mark;
            if (mark != TicTacToeMarks.Empty)
            {
                winner = mark.ToString();
                for (int i = 1; i < _grid.GetLength(0); i++)
                {
                    if (_grid[i, i].Mark != mark)
                    {
                        winner = "";
                        break;
                    }
                }
            }

            //Checking diagonal that goes from top right to bottom left
            if (winner.Equals(""))
            {
                mark = _grid[0, _grid.GetLength(0) - 1].Mark;
                if (mark != TicTacToeMarks.Empty)
                {
                    winner = mark.ToString();
                    for (int i = 1; i < _grid.GetLength(0); i++)
                    {
                        if (_grid[i, _grid.GetLength(0) - 1 - i].Mark != mark)
                        {
                            winner = "";
                            break;
                        }
                    }
                }
            }

            return winner;
        }

        public string CheckGrid()
        {
            string winner = CheckRows();
            if (winner != "") return winner;
            winner = CheckColumns();
            if (winner != "") return winner;
            winner = CheckDiagonals();
            if (winner != "") return winner;

            foreach (TicTacToePosition position in _grid)
            {
                if (position.Mark == TicTacToeMarks.Empty)
                {
                    return "Ongoing";
                }
            }

            return "Tie";
        }
    }

    public class TicTacToeGame
    {
        public static void Main(string[] args)
        {
            TicTacToeGrid grid = new TicTacToeGrid(3);
            Boolean endGame = false;
            TicTacToeMarks player = TicTacToeMarks.X;
            string result = "";
            while (!endGame)
            {
                Console.WriteLine("Player {0}, it's your turn to play.", player);
                grid.PrintGrid();
                PlayATurn(player, grid);
                player = player == TicTacToeMarks.X ? TicTacToeMarks.O : TicTacToeMarks.X;
                result = grid.Winner;
                if (!result.Equals("Ongoing"))
                {
                    endGame = true;
                }
            }

            if (result.Equals("Tie"))
            {
                Console.WriteLine("There is a tie.");
            }
            else
            {
                Console.WriteLine("The winner is player {0}. Congratulations!", result);
            }

        }

        public static void PlayATurn(TicTacToeMarks player, TicTacToeGrid grid)
        {
            Boolean endRound = false;
            while (!endRound)
            {
                int row = GetIntInput("Enter a row: ");
                int column = GetIntInput("Enter a column: ");
                endRound = grid.PlaceCharacter(player, row, column);
                if (!endRound)
                {
                    Console.WriteLine("Error placing '{0}'. Try another row and column.", player);
                }
            }
        }

        public static int GetIntInput(string message)
        {
            int? answer;
            do 
            {
                try
                {
                    Console.Write(message);
                    answer = Convert.ToInt32(Console.ReadLine());

                }
                catch (FormatException e)
                {
                    Console.Write("Your answer should be an integer.");
                    answer = null;
                }
            } while (answer == null);

            return (int) answer;
        }
    }
}