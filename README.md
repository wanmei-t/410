# 410

## Creating A Project
dotnet new console

dotnet new gitignore

dotnet run Program.cs

## Setting Up A Test Environment
In the debugger tab, click on Generate C# Assets For Build And Debug

Open the launch.json file in the .vscode folder and 

set “console” from “internalConsole” to “integratedTerminal”

dotnet new mstest -o TestProjectName -f "net6.0"

dotnet add ./TestProjectName/ProjectTests.csproj reference ./ProjectName/Project.csproj

dotnet test

dotnet test --filter ConcentrationGrid=Namespace.ConcentrationGridTest  